insert into pokemon (nivel,nombre) values(5,"Rattata");
insert into pokemon (nivel,nombre) values(7,"Bulbasaur");
insert into pokemon (nivel,nombre) values(2,"Magikarp");
insert into pokemon (nivel,nombre) values(6,"Pikachu");
insert into pokemon (nivel,nombre) values(9,"Mankey");
insert into pokemon (nivel,nombre) values(10,"Charmander");
insert into pokemon (nivel,nombre) values(2,"Squirtle");
insert into pokemon (nivel,nombre) values(1,"Caterpie");
insert into pokemon (nivel,nombre) values(5,"Pidgey");
insert into pokemon (nivel,nombre) values(1,"Ekans");
insert into tipo (nombre) values("Normal");
insert into tipo (nombre) values("Planta");
insert into tipo (nombre) values("Veneno");
insert into tipo (nombre) values("Agua");
insert into tipo (nombre) values("Electrico");
insert into tipo (nombre) values("Lucha");
insert into tipo (nombre) values("Fuego");
insert into tipo (nombre) values("Bicho");
insert into tipo (nombre) values("Volador");


insert into pokemon_tipo (pokemon_id,tipo_id) values(1,1);
insert into pokemon_tipo (pokemon_id,tipo_id) values(2,2);
insert into pokemon_tipo (pokemon_id,tipo_id) values(2,3);
insert into pokemon_tipo (pokemon_id,tipo_id) values(3,4);
insert into pokemon_tipo (pokemon_id,tipo_id) values(4,5);
insert into pokemon_tipo (pokemon_id,tipo_id) values(5,6);
insert into pokemon_tipo (pokemon_id,tipo_id) values(6,7);
insert into pokemon_tipo (pokemon_id,tipo_id) values(7,4);
insert into pokemon_tipo (pokemon_id,tipo_id) values(8,8);
insert into pokemon_tipo (pokemon_id,tipo_id) values(9,1);
insert into pokemon_tipo (pokemon_id,tipo_id) values(9,9);
insert into pokemon_tipo (pokemon_id,tipo_id) values(10,3);



insert into habilidad (nombre, descripcion) values("Agallas","Sube el ataque si sufre un problema de estado");
insert into habilidad (nombre, descripcion) values("Entusiasmo","Aumenta el ataque, pero reduce la precisión"); 
insert into habilidad (nombre, descripcion) values("Fuga","Permite escapar de todos los Pokémon salvajes");

insert into habilidad (nombre, descripcion) values("Espesura","Potencia los ataques de tipo planta en un apuro");
insert into habilidad (nombre, descripcion) values("Clorofila","Sube la velocidad cuando hay sol");

insert into habilidad (nombre, descripcion) values("Nado rápido","Sube la velocidad cuando hay lluvia");
insert into habilidad (nombre, descripcion) values("Cobardia","El miedo a algunos ataques sube su velocidad");

insert into habilidad (nombre, descripcion) values("Electricidad estática","Puede paralizar al mínimo contacto");
insert into habilidad (nombre, descripcion) values("Pararrayos","Atrae y neutraliza movimientos de tipo eléctrico y sube el ataque especial");

insert into habilidad (nombre, descripcion) values("Espíritu vital","Evita el quedarse dormido");
insert into habilidad (nombre, descripcion) values("Irascible","Sube el ataque al máximo tras un golpe crítico.");

insert into habilidad (nombre, descripcion) values("Mar llamas","Potencia los ataques de tipo fuego en un apuro");
insert into habilidad (nombre, descripcion) values("Poder solar","Si hace sol, baja los PS, pero potencia el ataque especial");

insert into habilidad (nombre, descripcion) values("Cura lluvia","Recupera PS de forma gradual cuando llueve");
insert into habilidad (nombre, descripcion) values("Torrente","Potencia los ataques de tipo agua en un apuro");

insert into habilidad (nombre, descripcion) values("Polvo escudo","Anula el efecto secundario del ataque sufrido");

insert into habilidad (nombre, descripcion) values("Sacapecho","Protege de los ataques que bajan la defensa");

insert into habilidad (nombre, descripcion) values("Mudar","Cura los problemas de estado mudando la piel");

insert into pokemon_habilidad(habilidad_id,pokemon_id) values(1,1);
insert into pokemon_habilidad(habilidad_id,pokemon_id) values(2,1);
insert into pokemon_habilidad(habilidad_id,pokemon_id) values(3,1);

insert into pokemon_habilidad(habilidad_id,pokemon_id) values(4,2);
insert into pokemon_habilidad(habilidad_id,pokemon_id) values(5,2);

insert into pokemon_habilidad(habilidad_id,pokemon_id) values(6,3);
insert into pokemon_habilidad(habilidad_id,pokemon_id) values(7,3);

insert into pokemon_habilidad(habilidad_id,pokemon_id) values(8,4);
insert into pokemon_habilidad(habilidad_id,pokemon_id) values(9,4);

insert into pokemon_habilidad(habilidad_id,pokemon_id) values(10,5);
insert into pokemon_habilidad(habilidad_id,pokemon_id) values(11,5);

insert into pokemon_habilidad(habilidad_id,pokemon_id) values(12,6);
insert into pokemon_habilidad(habilidad_id,pokemon_id) values(13,6);

insert into pokemon_habilidad(habilidad_id,pokemon_id) values(14,7);
insert into pokemon_habilidad(habilidad_id,pokemon_id) values(15,7);

insert into pokemon_habilidad(habilidad_id,pokemon_id) values(16,8);
insert into pokemon_habilidad(habilidad_id,pokemon_id) values(17,9);

insert into pokemon_habilidad(habilidad_id,pokemon_id) values(18,10);
insert into pokemon_habilidad(habilidad_id,pokemon_id) values(3,8);

insert into usuario(nombre) values("Cristian");
insert into usuario(nombre) values("Pepe");

insert into pokemon_usuario(pokemon_id,usuario_id) values(1,1);
insert into pokemon_usuario(pokemon_id,usuario_id) values(2,1);
insert into pokemon_usuario(pokemon_id,usuario_id) values(2,2);

insert into evolucion (nivel_evolucion,nombre,tipo,pokemon_id) values(20,"Raticate","Por la noche",1);

insert into evolucion (nivel_evolucion,nombre,tipo,pokemon_id) values(16,"Ivysaur","Normal",2);
insert into evolucion (nivel_evolucion,nombre,tipo,pokemon_id) values(32,"Venusaur","Normal",2);

insert into evolucion (nivel_evolucion,nombre,tipo,pokemon_id) values(20,"Gyarados","Normal",3);

insert into evolucion (nivel_evolucion,nombre,tipo,pokemon_id) values(28,"Primeape","Normal",5);

insert into evolucion (nivel_evolucion,nombre,tipo,pokemon_id) values(16,"Charmeleon","Normal",6);
insert into evolucion (nivel_evolucion,nombre,tipo,pokemon_id) values(36,"Charizard","Normal",6);

insert into evolucion (nivel_evolucion,nombre,tipo,pokemon_id) values(16,"Wartortle","Normal",7);
insert into evolucion (nivel_evolucion,nombre,tipo,pokemon_id) values(36,"Blastoise","Normal",7);

insert into evolucion (nivel_evolucion,nombre,tipo,pokemon_id) values(7,"Metapod","Normal",8);
insert into evolucion (nivel_evolucion,nombre,tipo,pokemon_id) values(10,"Butterfree","Normal",8);

insert into evolucion (nivel_evolucion,nombre,tipo,pokemon_id) values(18,"Pidgeotto","Normal",9);
insert into evolucion (nivel_evolucion,nombre,tipo,pokemon_id) values(36,"Pidgeot","Por la mañana",9);
insert into evolucion (nivel_evolucion,nombre,tipo,pokemon_id) values(22,"Arbok","Normal",10);



