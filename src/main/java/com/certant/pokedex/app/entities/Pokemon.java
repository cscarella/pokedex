package com.certant.pokedex.app.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "pokemon")
public class Pokemon {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	private String nombre;
	
	private int nivel;

	public Pokemon() {
	}

	


	public Pokemon(int id, String nombre, int nivel) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.nivel = nivel;
	}




	public Pokemon(String nombre, int nivel) {
		super();
		this.nombre = nombre;
		this.nivel = nivel;
	}



	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}



	public int getNivel() {
		return nivel;
	}



	public void setNivel(int nivel) {
		this.nivel = nivel;
	}



	@Override
	public String toString() {
		return "id= " + id + ", nombre= " + nombre + ", nivel= " + nivel;
	}
	
	
	
	
	
}
