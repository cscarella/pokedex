package com.certant.pokedex.app.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "habilidad")
public class Habilidad {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	private String nombre;
	
	private String descripcion;

	public Habilidad() {
	}

	public Habilidad(String nombre, String descripcion) {
		setNombre(nombre);
		setDescripcion(descripcion);
	}
	

	public Habilidad(int id, String nombre, String descripcion) {
		this(nombre,descripcion);
		setId(id);
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getId() {
		return id;
	}
	

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "nombre= " + nombre + ", descripcion= " + descripcion;
	}
	
	
}
