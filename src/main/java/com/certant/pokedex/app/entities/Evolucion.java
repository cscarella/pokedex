package com.certant.pokedex.app.entities;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name = "evolucion")
public class Evolucion {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	private String nombre;
	
	private String tipo;
	
	private int nivelEvolucion;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private Pokemon pokemon;

	public Evolucion() {
	}

	public Evolucion(String nombre, String tipo, int nivelEvolucion, Pokemon pokemon) {
		setNombre(nombre);
		setTipo(tipo);
		setNivelEvolucion(nivelEvolucion);
		setPokemon(pokemon);
	}

	
	
	public Evolucion(int id, String nombre, String tipo, int nivelEvolucion, Pokemon pokemon) {
		this(nombre,tipo,nivelEvolucion,pokemon);
		setId(id);
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public int getNivelEvolucion() {
		return nivelEvolucion;
	}

	public void setNivelEvolucion(int nivelEvolucion) {
		this.nivelEvolucion = nivelEvolucion;
	}

	public Pokemon getPokemon() {
		return pokemon;
	}

	public void setPokemon(Pokemon pokemon) {
		this.pokemon = pokemon;
	}

	public int getId() {
		return id;
	}

	@Override
	public String toString() {
		return "nombre= " + nombre + ", tipo= " + tipo + ", nivelEvolucion= " + nivelEvolucion;
	}
	
	
	
	
}
