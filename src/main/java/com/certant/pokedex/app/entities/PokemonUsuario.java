package com.certant.pokedex.app.entities;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name = "pokemon_usuario")
public class PokemonUsuario {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.PERSIST)
    @OnDelete(action = OnDeleteAction.CASCADE)
	private Pokemon pokemon;
	
	@ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.PERSIST)
    @OnDelete(action = OnDeleteAction.CASCADE)
	private Usuario usuario;
	
	

	public PokemonUsuario() {
	}



	public PokemonUsuario(int id, Pokemon pokemon, Usuario usuario) {
		super();
		this.id = id;
		this.pokemon = pokemon;
		this.usuario = usuario;
	}



	public Pokemon getPokemon() {
		return pokemon;
	}



	public void setPokemon(Pokemon pokemon) {
		this.pokemon = pokemon;
	}



	public Usuario getUsuario() {
		return usuario;
	}



	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}




	public int getId() {
		return id;
	}


	
	
}
