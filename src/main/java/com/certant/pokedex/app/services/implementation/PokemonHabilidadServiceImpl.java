package com.certant.pokedex.app.services.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.certant.pokedex.app.entities.PokemonHabilidad;
import com.certant.pokedex.app.repositories.IPokemonHabilidadRepository;
import com.certant.pokedex.app.services.IPokemonHabilidadService;

@Service("pokemonHabilidadService")
public class PokemonHabilidadServiceImpl implements IPokemonHabilidadService{

	@Autowired
	@Qualifier("pokemonHabilidadRepository")
	private IPokemonHabilidadRepository pokemonHabilidadRepository;
	
	@Override
	public void deleteAllHabilidadPokemon(int id) {
		for(PokemonHabilidad pokemonHabilidad: pokemonHabilidadRepository.obtenerPokemonesPorHabilidad(id)) {
			pokemonHabilidadRepository.delete(pokemonHabilidad);
		}
	}

}
