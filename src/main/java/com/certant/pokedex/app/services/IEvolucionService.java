package com.certant.pokedex.app.services;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import com.certant.pokedex.app.entities.Evolucion;

public interface IEvolucionService {
	
	public Evolucion insertEvolucion(Evolucion evolucion);
	
	public void delete(int id);

	public Optional<Evolucion> findById(int id) throws NoSuchElementException;

	public List<Evolucion> findAll();
}
