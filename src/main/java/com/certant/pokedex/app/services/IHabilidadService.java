package com.certant.pokedex.app.services;

import java.util.List;

import com.certant.pokedex.app.entities.Habilidad;


public interface IHabilidadService {

	public List<Habilidad> getAll();
	
	public Habilidad findById(int id);
}
