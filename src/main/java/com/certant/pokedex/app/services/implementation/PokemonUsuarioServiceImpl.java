package com.certant.pokedex.app.services.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.certant.pokedex.app.entities.PokemonUsuario;
import com.certant.pokedex.app.repositories.IPokemonUsuarioRepository;
import com.certant.pokedex.app.services.IPokemonUsuarioService;

@Service("pokemonUsuarioService")
public class PokemonUsuarioServiceImpl implements IPokemonUsuarioService{


	@Autowired
	@Qualifier("pokemonUsuarioRepository")
	private IPokemonUsuarioRepository pokemonUsuarioRepository;
	
	
	@Override
	public List<PokemonUsuario> getAllPokemonesByUsuario(int idUsuario) {
		return pokemonUsuarioRepository.obtenerPokemonesPorUsuario(idUsuario);
	}

}
