package com.certant.pokedex.app.services.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.certant.pokedex.app.entities.Usuario;
import com.certant.pokedex.app.repositories.IUsuarioRepository;
import com.certant.pokedex.app.services.IUsuarioService;

@Service("usuarioService")
public class UsuarioServiceImpl implements IUsuarioService{

	@Autowired
	@Qualifier("usuarioRepository")
	private IUsuarioRepository usuarioRepository;
	
	@Override
	public Usuario findById(int id) {
		return usuarioRepository.findById(id).orElse(null);
	}

}
