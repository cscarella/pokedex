package com.certant.pokedex.app.services.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.certant.pokedex.app.entities.PokemonTipo;
import com.certant.pokedex.app.entities.Tipo;
import com.certant.pokedex.app.repositories.IPokemonTipoRepository;
import com.certant.pokedex.app.services.IPokemonTipoService;

@Service("pokemonTipoService")
public class PokemonTipoServiceImpl implements IPokemonTipoService{

	@Autowired
	@Qualifier("pokemonTipoRepository")
	private IPokemonTipoRepository pokemonTipoRepository;
	
	@Override
	public void deleteAllByPokemon(int id) {
		for(PokemonTipo pokemonTipo: pokemonTipoRepository.obtenerPokemonesPorTipo(id)) {
			pokemonTipoRepository.delete(pokemonTipo);
		}
	}

	@Override
	public List<Tipo> obtenerTiposPorPokemon(int pokemonId) {
		
		return pokemonTipoRepository.obtenerTiposPorPokemon(pokemonId);
	}

}
