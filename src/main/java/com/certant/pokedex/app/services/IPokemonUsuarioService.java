package com.certant.pokedex.app.services;

import java.util.List;

import com.certant.pokedex.app.entities.PokemonUsuario;

public interface IPokemonUsuarioService {

	public List<PokemonUsuario> getAllPokemonesByUsuario(int idUsuario);
}
