package com.certant.pokedex.app.services.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.certant.pokedex.app.entities.Tipo;
import com.certant.pokedex.app.repositories.ITipoRepository;
import com.certant.pokedex.app.services.ITipoService;

@Service("tipoService")
public class TipoServiceImpl implements ITipoService{

	@Autowired
	@Qualifier("tipoRepository")
	private ITipoRepository tipoRepository;
	
	@Override
	public List<Tipo> getAll() {
		return tipoRepository.getAllTipos();
	}
	


	@Override
	public Tipo findById(int id) {
		return tipoRepository.findById(id);
	}
	
	@Override
	public Tipo findByNombre(String nombre) {
		return tipoRepository.findByNombre(nombre);
	}

}
