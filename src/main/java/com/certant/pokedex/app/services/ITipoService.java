package com.certant.pokedex.app.services;

import java.util.List;

import com.certant.pokedex.app.entities.Tipo;

public interface ITipoService {

	public List<Tipo> getAll();
	
	public Tipo findById(int id);

	public Tipo findByNombre(String nombre);
}
