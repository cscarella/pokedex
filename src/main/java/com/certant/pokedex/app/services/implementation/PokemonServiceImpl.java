package com.certant.pokedex.app.services.implementation;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.certant.pokedex.app.entities.Evolucion;
import com.certant.pokedex.app.entities.Habilidad;
import com.certant.pokedex.app.entities.Pokemon;
import com.certant.pokedex.app.entities.PokemonHabilidad;
import com.certant.pokedex.app.entities.PokemonTipo;
import com.certant.pokedex.app.entities.Tipo;
import com.certant.pokedex.app.models.PokemonModel;
import com.certant.pokedex.app.repositories.IEvolucionRepository;
import com.certant.pokedex.app.repositories.IPokemonHabilidadRepository;
import com.certant.pokedex.app.repositories.IPokemonRepository;
import com.certant.pokedex.app.repositories.IPokemonTipoRepository;
import com.certant.pokedex.app.services.IHabilidadService;
import com.certant.pokedex.app.services.IPokemonHabilidadService;
import com.certant.pokedex.app.services.IPokemonService;
import com.certant.pokedex.app.services.IPokemonTipoService;
import com.certant.pokedex.app.services.ITipoService;

@Service("pokemonService")
public class PokemonServiceImpl implements IPokemonService {

	@Autowired
	@Qualifier("pokemonRepository")
	private IPokemonRepository pokemonRepository;

	@Autowired
	@Qualifier("pokemonHabilidadRepository")
	private IPokemonHabilidadRepository pokemonHabilidadRepository;

	@Autowired
	@Qualifier("evolucionRepository")
	private IEvolucionRepository evolucionRepository;

	@Autowired
	@Qualifier("pokemonTipoRepository")
	private IPokemonTipoRepository pokemonTipoRepository;

	@Autowired
	@Qualifier("tipoService")
	private ITipoService tipoService;

	@Autowired
	@Qualifier("habilidadService")
	private IHabilidadService habilidadService;

	@Autowired
	@Qualifier("pokemonTipoService")
	private IPokemonTipoService pokemonTipoService;
	
	@Autowired
	@Qualifier("pokemonHabilidadService")
	private IPokemonHabilidadService pokemonHabilidadService;


	public PokemonServiceImpl(IPokemonRepository pokemonRepository) {
		super();
		this.pokemonRepository = pokemonRepository;
	}

	@Override
	public List<Pokemon> getAllPokemon() {
		return pokemonRepository.getAllPokemon();
	}

	@Override
	public List<PokemonTipo> getAllTiposPokemon(int idPokemon) {
		return pokemonRepository.getAllTiposPokemon(idPokemon);
	}

	@Override
	public List<PokemonHabilidad> obtenerHabilidadesPokemon(int idPokemon) {
		return pokemonHabilidadRepository.obtenerPokemonesPorHabilidad(idPokemon);
	}

	@Override
	public List<Evolucion> obtenerEvolucionesPokemon(int idPokemon) {
		return evolucionRepository.obtenerEvolucionesPokemon(idPokemon);
	}

	@Override
	public Pokemon findById(int id) throws NoSuchElementException{
		return pokemonRepository.findById(id).orElseThrow(() -> new NoSuchElementException("No existe el pokemon"));
	}

	@Override
	@Transactional
	public void insertOrUpdate(PokemonModel model) throws NoSuchElementException{
		Pokemon pokemon=null;
		boolean guardar=true;
		PokemonTipo tipo = null;
		PokemonHabilidad habilidad = null;

		if (model.getNombre()!=null && model.getNivel()>=0 && (model.getTipos()!=null && !model.getTipos().isEmpty())) {
			if(model.getId()!=0) {
				pokemon = findById(model.getId());
				pokemonTipoService.deleteAllByPokemon(pokemon.getId());
				pokemonHabilidadService.deleteAllHabilidadPokemon(pokemon.getId());
			}else {
				pokemon=new Pokemon();
			}
			pokemon.setNombre(model.getNombre());
			pokemon.setNivel(model.getNivel());

			for (Tipo tipoModel : model.getTipos()) {
				Tipo tipoI= tipoService.findById(tipoModel.getId());
				if(tipoI!=null) {
					if(guardar) {  //esto se hace 1 sola vez y es cuando en la BDD se encuentra al menos 1 tipo ingresado
						pokemonRepository.save(pokemon);
						guardar=false;
					}
				tipo = new PokemonTipo(pokemon, tipoI);
				pokemonTipoRepository.save(tipo);
				}
			}
			if(guardar) //esto hace que si el idTipo no existe, guardar queda en true y lanza la excepcion
				throw new NoSuchElementException("No se pudo insertar, o actualizar el pokemon porque los tipos ingresados no existen en la BDD");
			if((model.getHabilidades()!=null && !model.getHabilidades().isEmpty())) {
			for (Habilidad habilidadModel : model.getHabilidades()) {
				habilidad = new PokemonHabilidad(pokemon, habilidadService.findById(habilidadModel.getId()));
				pokemonHabilidadRepository.save(habilidad);

			}
			}
		}else {
			throw new NoSuchElementException("No se pudo insertar, o actualizar el pokemon");
		}
		
	}//primero consultar si el model tiene tipos y despues buscar por id cada tipo y si se encuentra alguno, se inserta
	//el pokemon correctamente. Sino lanzo una excepcion.


	@Override
	public void delete(int id) throws NoSuchElementException{
		if(findById(id)!=null) {
			pokemonRepository.deleteById(id);
		}
	}

	@Override
	public Optional<Pokemon> findByNombre(String nombre) {
		return pokemonRepository.findByNombre(nombre);
	}

}
