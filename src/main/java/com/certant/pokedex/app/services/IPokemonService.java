package com.certant.pokedex.app.services;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import com.certant.pokedex.app.entities.Evolucion;
import com.certant.pokedex.app.entities.Pokemon;
import com.certant.pokedex.app.entities.PokemonHabilidad;
import com.certant.pokedex.app.entities.PokemonTipo;
import com.certant.pokedex.app.models.PokemonModel;

public interface IPokemonService {
	public List<Pokemon> getAllPokemon();
	
	public List<PokemonTipo> getAllTiposPokemon(int idPokemon);
	
	public List<PokemonHabilidad> obtenerHabilidadesPokemon(int idPokemon);
	
	public List<Evolucion> obtenerEvolucionesPokemon(int idPokemon);
	
	public void insertOrUpdate(PokemonModel model) throws NoSuchElementException;
	
	public void delete(int id) throws NoSuchElementException;
	
	public Pokemon findById(int id) throws NoSuchElementException;
	
	public Optional<Pokemon> findByNombre(String nombre);
	
}
