package com.certant.pokedex.app.services.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.certant.pokedex.app.entities.Habilidad;
import com.certant.pokedex.app.repositories.IHabilidadRepository;
import com.certant.pokedex.app.services.IHabilidadService;

@Service("habilidadService")
public class HabilidadServiceImpl implements IHabilidadService{

	@Autowired
	@Qualifier("habilidadRepository")
	private IHabilidadRepository habilidadRepository;
	
	@Override
	public List<Habilidad> getAll() {
		return habilidadRepository.getAllHabilidad();
	}

	@Override
	public Habilidad findById(int id) {
		return habilidadRepository.findById(id);
	}
	



}
