package com.certant.pokedex.app.services;

import java.util.List;

import com.certant.pokedex.app.entities.Tipo;

public interface IPokemonTipoService {

	public void deleteAllByPokemon(int id);
	
	public List<Tipo> obtenerTiposPorPokemon(int pokemonId);
}
