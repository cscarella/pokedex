package com.certant.pokedex.app.services;

import com.certant.pokedex.app.entities.Usuario;

public interface IUsuarioService {

	public Usuario findById(int id);
}
