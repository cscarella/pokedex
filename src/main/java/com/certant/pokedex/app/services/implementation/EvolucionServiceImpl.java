package com.certant.pokedex.app.services.implementation;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.certant.pokedex.app.entities.Evolucion;
import com.certant.pokedex.app.repositories.IEvolucionRepository;
import com.certant.pokedex.app.services.IEvolucionService;

@Service("evolucionService")
public class EvolucionServiceImpl implements IEvolucionService{

	@Autowired
	@Qualifier("evolucionRepository")
	private IEvolucionRepository evolucionRepository;
	
	@Override
	public Evolucion insertEvolucion(Evolucion evolucion) {
		if(evolucion.getNombre()!=null && evolucion.getPokemon()!=null && evolucion.getNivelEvolucion()!=0 && evolucion.getTipo()!=null) {
			return evolucionRepository.save(evolucion);
		}
		return null;
	}
	
	@Override
	public Optional<Evolucion> findById(int id) throws NoSuchElementException{
		Optional<Evolucion> evolucion = evolucionRepository.findById(id);
		if(evolucion.isPresent()) {
			return evolucion;
		}
		else {
			throw new NoSuchElementException("La evolucion con el id: "+ id+" no existe.");
		}
	}

	@Override
	public void delete(int id) {
		Optional<Evolucion> evolucion = evolucionRepository.findById(id);
		if(evolucion.isPresent()) {
			evolucionRepository.deleteById(id);
		}
	}
	
	@Override
	public List<Evolucion> findAll() {
		return evolucionRepository.findAll();
	}

}
