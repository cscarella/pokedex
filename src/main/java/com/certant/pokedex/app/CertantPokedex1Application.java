package com.certant.pokedex.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CertantPokedex1Application {

	public static void main(String[] args) {
		SpringApplication.run(CertantPokedex1Application.class, args);
	}

}
