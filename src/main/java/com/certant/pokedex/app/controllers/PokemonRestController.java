package com.certant.pokedex.app.controllers;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.certant.pokedex.app.entities.Evolucion;
import com.certant.pokedex.app.entities.Pokemon;
import com.certant.pokedex.app.entities.Tipo;
import com.certant.pokedex.app.models.PokemonModel;
import com.certant.pokedex.app.services.IEvolucionService;
import com.certant.pokedex.app.services.IPokemonService;
import com.certant.pokedex.app.services.IPokemonTipoService;

@RestController
@RequestMapping("/api")
public class PokemonRestController {

	Logger logger = LoggerFactory.getLogger(PokemonRestController.class);
	
	@Autowired
	@Qualifier("pokemonService")
	private IPokemonService pokemonService;

	@Autowired
	@Qualifier("evolucionService")
	private IEvolucionService evolucionService;
	
	@Autowired
	@Qualifier("pokemonTipoService")
	private IPokemonTipoService pokemonTipoService;
	
	@GetMapping("/{id}")
	public Pokemon findById(@PathVariable int id) {
		return pokemonService.findById(id);
	}

	@GetMapping("/listar")
	public List<Pokemon> listar() {
		return pokemonService.getAllPokemon();
	}

	@DeleteMapping("/eliminarPokemon/{id}")
	public String eliminar(@PathVariable int id) {
		String message = "Se eliminó correctamente";
		try {
			pokemonService.delete(id);
		} catch (NoSuchElementException e) {
			logger.error(e.getMessage());
			message = "El pokemon no se pudo eliminar, o no existe con el id: " + id;
		}
		return message;
	}

	@GetMapping("/listarEvoluciones")
	public List<Evolucion> listarEvoluciones() {
		return evolucionService.findAll();
	}

	@PostMapping("/agregarEvolucion")
	public void agregarEvolucion(@RequestBody Evolucion evolucion) {
		evolucionService.insertEvolucion(evolucion);
	}
	
	@GetMapping("/listarTiposPokemon/{id}")
	public List<Tipo> listarTiposPokemon(@PathVariable int id){
		return pokemonTipoService.obtenerTiposPorPokemon(id);
	}

	@GetMapping("/evolucion/{id}")
	@ResponseStatus(value=HttpStatus.OK)
	public Evolucion findByEvolucion(@PathVariable int id) {
		Evolucion evolucion=null;
		try {
			Optional<Evolucion> evolucionOptional = evolucionService.findById(id);
			if(evolucionOptional.isPresent()) {
				evolucion= evolucionOptional.get();
			}
		}catch(NoSuchElementException e) {
			logger.error(e.getMessage());
		}
		return evolucion;
	}
	
	@PostMapping("/agregar")
	public ResponseEntity<?> insert(@RequestBody PokemonModel pokemonModel){
		String message = "Se insertó correctamente";
		try {
			pokemonService.insertOrUpdate(pokemonModel);
		}catch(NoSuchElementException e){
			message = e.getMessage();
		}
		Map<String, Object> response = new HashMap<>();
		response.put("date", LocalDate.now().toString());
		response.put("status", "Ok");
		response.put("mensaje", message);
		response.put("insert",pokemonModel);
		return ResponseEntity.ok(response);
	}
}
