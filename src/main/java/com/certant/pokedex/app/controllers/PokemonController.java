package com.certant.pokedex.app.controllers;

import java.util.List;
import java.util.NoSuchElementException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import com.certant.pokedex.app.entities.Evolucion;
import com.certant.pokedex.app.entities.Habilidad;
import com.certant.pokedex.app.entities.Pokemon;
import com.certant.pokedex.app.entities.Tipo;
import com.certant.pokedex.app.models.PokemonModel;
import com.certant.pokedex.app.models.editors.HabilidadEditors;
import com.certant.pokedex.app.models.editors.PokemonEditors;
import com.certant.pokedex.app.services.IEvolucionService;
import com.certant.pokedex.app.services.IHabilidadService;
import com.certant.pokedex.app.services.IPokemonService;
import com.certant.pokedex.app.services.IPokemonTipoService;
import com.certant.pokedex.app.services.ITipoService;
import static com.certant.pokedex.app.helpers.ViewRouteHelper.*;

@Controller
@RequestMapping("/app")
public class PokemonController {

	@Autowired
	@Qualifier("pokemonService")
	private IPokemonService pokemonService;
	
	@Autowired
	@Qualifier("tipoService")
	private ITipoService tipoService;
	
	@Autowired
	@Qualifier("habilidadService")
	private IHabilidadService habilidadService;
	
	@Autowired
	@Qualifier("pokemonTipoService")
	private IPokemonTipoService pokemonTipoService;
	
	@Autowired
	@Qualifier("evolucionService")
	private IEvolucionService evolucionService;
	
	@Autowired
	private PokemonEditors pokemonEditors;
	
	@Autowired
	private HabilidadEditors habilidadEditors;
	
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		
		binder.registerCustomEditor(Tipo.class, "tipos", pokemonEditors);
		binder.registerCustomEditor(Habilidad.class, "habilidades", habilidadEditors);
	}
	
	@GetMapping("/listar")
	public String listar(Model model) {
		model.addAttribute("pokemones",pokemonService.getAllPokemon());
		return "listar";
	}
	
	@GetMapping("/tipo/{id}")
	public String tipos(@PathVariable(value="id")int id,Model model) {
		model.addAttribute("tipos",pokemonService.getAllTiposPokemon(id));
		return "tipo";
	}
	
	@GetMapping("/habilidades/{id}")
	public String habilidades(@PathVariable(value="id")int id,Model model) {
		model.addAttribute("habilidades", pokemonService.obtenerHabilidadesPokemon(id));
		return "habilidad";
	}
	
	@GetMapping("/evoluciones/{id}")
	public String evoluciones(@PathVariable(value="id")int id,Model model) {
		model.addAttribute("evoluciones", pokemonService.obtenerEvolucionesPokemon(id));
		return "evolucion";
	}
	
	@ModelAttribute("listaTipos")
	public List<Tipo> listaTipos() {
		return tipoService.getAll();
	}
	
	@ModelAttribute("listaHabilidades")
	public List<Habilidad> listaHabilidades() {
		return habilidadService.getAll();
	}
	
	@GetMapping("/agregar")
	public String agregarPokemon(Model model) {
		PokemonModel pokemonModel = new PokemonModel();
		model.addAttribute("pokemonModel", pokemonModel);
		return "pokemon/agregar";
	}
	
	@PostMapping("/agregar")
	public String agregarPokemones(@ModelAttribute("pokemonModel") PokemonModel pokemonModel) throws NoSuchElementException {
		try {
			pokemonService.insertOrUpdate(pokemonModel);
		} catch (NoSuchElementException e) {
			e.getMessage();
		}
		return REDIRECT_LISTAR;
	}
	
	@GetMapping("/editar/{id}")// el {id} tiene que ser igual al value
	public String editar(@PathVariable(value="id")int id, Model model) {
		PokemonModel pokemonModel= new PokemonModel();
		if(id>0) {
			Pokemon pokemon=pokemonService.findById(id);
			pokemonModel.setId(id);
			pokemonModel.setNombre(pokemon.getNombre());
			pokemonModel.setNivel(pokemon.getNivel());
		}else {
			return "redirect:/listar";
		}
		model.addAttribute("pokemonModel", pokemonModel);
		return "pokemon/agregar";
	}
	
	
	@GetMapping("/eliminar/{id}")
	public String eliminar(@PathVariable(value="id") int id) {
		if(id > 0) {

			pokemonService.delete(id);
		}
		return REDIRECT_LISTAR;
	}
	
	@GetMapping("/agregarEvolucion/{id}")
	public String agregarEvolucion(@PathVariable(value="id")int id,Model model) {
		Evolucion evolucion = new Evolucion();
		evolucion.setPokemon(pokemonService.findById(id));
		model.addAttribute("evolucion", evolucion);
		return "agregarEvolucion";
	}
	
	@PostMapping("/agregarEvolucion")
	public String agregarEvoluciones(@ModelAttribute("evolucion") Evolucion evolucion) {
		evolucionService.insertEvolucion(evolucion);
		return REDIRECT_LISTAR;
	}
}
