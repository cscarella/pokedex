package com.certant.pokedex.app.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import com.certant.pokedex.app.services.IPokemonUsuarioService;
import com.certant.pokedex.app.services.IUsuarioService;

@Controller
@RequestMapping("/app")
public class PokemonUsuarioController {
	
	@Autowired
	@Qualifier("pokemonUsuarioService")
	private  IPokemonUsuarioService pokemonUsuarioService;

	@Autowired
	@Qualifier("usuarioService")
	private  IUsuarioService usuarioService;
	
	@GetMapping("/usuario/{id}")
	public String listarPokemonesPorUsuario(@PathVariable(value="id")int id,Model model) {
		if(usuarioService.findById(id)==null) {
			return "redirect:/app/listar";
		}
		model.addAttribute("usuario", usuarioService.findById(id));
		model.addAttribute("pokemones",pokemonUsuarioService.getAllPokemonesByUsuario(id));
		return "/usuario/listar";
	}
}
