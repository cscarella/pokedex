package com.certant.pokedex.app.repositories;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.certant.pokedex.app.entities.Tipo;

@Repository("tipoRepository")
public interface ITipoRepository extends JpaRepository<Tipo, Serializable>{

	public abstract Tipo findById(int id);
	
	@Query("Select t from Tipo t")
	public abstract List<Tipo> getAllTipos();

	@Query("Select t from Tipo t where t.nombre=(:nombre)")
	public abstract Tipo findByNombre(String nombre);

}
