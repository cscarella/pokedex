package com.certant.pokedex.app.repositories;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.certant.pokedex.app.entities.Habilidad;

@Repository("habilidadRepository")
public interface IHabilidadRepository extends JpaRepository<Habilidad, Serializable>{
	
	public abstract Habilidad findById(int id);
	
	@Query("Select h from Habilidad h")
	public abstract List<Habilidad> getAllHabilidad();
	

	
}
