package com.certant.pokedex.app.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.certant.pokedex.app.entities.Evolucion;

@Repository("evolucionRepository")
public interface IEvolucionRepository extends JpaRepository<Evolucion, Integer>{

	@Query("Select e from Evolucion e"
			+ " JOIN e.pokemon p"
			+ " where p.id=(:pokemonId)")
	public abstract List<Evolucion> obtenerEvolucionesPokemon(int pokemonId);
	
	@Query("Select e from Evolucion e"
			+ " JOIN e.pokemon p"
			+ " where p.nombre=(:nombrePokemon)")
	public abstract List<Evolucion> obtenerEvolucionesPokemon(String nombrePokemon);
	
	
}
