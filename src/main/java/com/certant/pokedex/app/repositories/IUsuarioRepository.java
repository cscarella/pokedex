package com.certant.pokedex.app.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.certant.pokedex.app.entities.Usuario;

@Repository("usuarioRepository")
public interface IUsuarioRepository extends PagingAndSortingRepository<Usuario, Integer>{

}
