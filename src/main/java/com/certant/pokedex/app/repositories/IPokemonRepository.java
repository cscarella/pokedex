package com.certant.pokedex.app.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.certant.pokedex.app.entities.Pokemon;
import com.certant.pokedex.app.entities.PokemonTipo;


@Repository("pokemonRepository")
public interface IPokemonRepository extends PagingAndSortingRepository<Pokemon, Integer>{
	
	@Query("SELECT p FROM Pokemon p")
	public abstract List<Pokemon> getAllPokemon();
	
	
	@Query("Select po from PokemonTipo po"
			+ " JOIN po.pokemon p "
			+ "JOIN po.tipo t where p.id=(:pokemonId)")
	public abstract List<PokemonTipo> getAllTiposPokemon(int pokemonId);
	
	@Query("SELECT p FROM Pokemon p where p.nombre=(:nombre)")
	public abstract Optional<Pokemon> findByNombre(String nombre);
}
