package com.certant.pokedex.app.repositories;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.certant.pokedex.app.entities.PokemonUsuario;

@Repository("pokemonUsuarioRepository")
public interface IPokemonUsuarioRepository extends JpaRepository<PokemonUsuario, Serializable> {

	@Query("Select pu from PokemonUsuario pu" + " JOIN pu.pokemon p " + "JOIN pu.usuario u where u.id=(:pokemonId)")
	public abstract List<PokemonUsuario> obtenerPokemonesPorUsuario(int pokemonId);
}
