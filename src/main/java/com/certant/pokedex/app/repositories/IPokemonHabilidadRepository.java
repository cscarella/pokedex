package com.certant.pokedex.app.repositories;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.certant.pokedex.app.entities.PokemonHabilidad;

@Repository("pokemonHabilidadRepository")
public interface IPokemonHabilidadRepository extends JpaRepository<PokemonHabilidad, Serializable>{

	@Query("Select ph from PokemonHabilidad ph JOIN ph.pokemon p where p.id=(:pokemonId)")
	public abstract List<PokemonHabilidad> obtenerPokemonesPorHabilidad(int pokemonId);
	
	@Query("Select ph from PokemonHabilidad ph JOIN ph.pokemon p where p.nombre=(:pokemon)")
	public abstract List<PokemonHabilidad> findPokemonesHabilidadByNombre(String pokemon);
	
}
