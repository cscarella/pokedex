package com.certant.pokedex.app.repositories;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import com.certant.pokedex.app.entities.Evolucion;
import com.certant.pokedex.app.entities.Habilidad;
import com.certant.pokedex.app.entities.Pokemon;
import com.certant.pokedex.app.entities.PokemonHabilidad;
import com.certant.pokedex.app.entities.PokemonTipo;
import com.certant.pokedex.app.entities.PokemonUsuario;
import com.certant.pokedex.app.entities.Tipo;
import com.certant.pokedex.app.entities.Usuario;

public class Datos {
	
	private Datos() {
		
	}
	
	public static final Optional<Pokemon> RATTATA=Optional.of(new Pokemon(1,"Rattata", 5));

	public static final List<Pokemon> POKEMONES=Arrays.asList(RATTATA.get(),new Pokemon(2,"Bulbasaur", 7));
	
	public static final Pokemon CHARIZAR = new Pokemon("Charizar", 42);
	
	public static final Evolucion EVOLUCION = new Evolucion("Evolucion Mayor", "Terrestre", 25, CHARIZAR);
	
	public static final List<Tipo> TIPOS_RATTATA=Arrays.asList(new Tipo(1,"Normal"));
	
	public static final List<Pokemon> LISTA_POKEMONES_BDD=Arrays.asList(	
			RATTATA.get(),
			new Pokemon("Bulbasaur", 7),
			new Pokemon("Magikarp", 2),
			new Pokemon("Pikachu", 6),
			new Pokemon("Mankey", 9),
			new Pokemon("Charmander", 10),
			new Pokemon("Squirtle", 2),
			new Pokemon("Caterpie", 1),
			new Pokemon("Pidgey", 5),
			new Pokemon("Ekans", 1));
	
	public static final Pokemon RATTATA_ROJO_FALLO = new Pokemon("Rattata Rojo", 13);
	
	public static final Optional<Evolucion> EVOLUCION_RATTATA = Optional.of(new Evolucion(1, "Raticate", "Por la noche", 20, RATTATA.get()));

	public static final Pokemon RATTATA_AZUL = new Pokemon("Rattata Azul", 23);
	
	public static final Tipo TIPO_RATTATA_AZUL = new Tipo(1, "Tierra");
	
	public static final List<Tipo> RATTATA_AZUL_TIPOS = Arrays.asList(TIPO_RATTATA_AZUL);
	
	public static final Habilidad HABILIDAD_RATTATA_AZUL = new Habilidad(1,"Clorofilia", "Sube la velocidad cuando hay sol");
	
	public static final List<Habilidad> RATTATA_AZUL_HABILIDADES = Arrays.asList(HABILIDAD_RATTATA_AZUL);
	
	//----
	
	public static final List<PokemonHabilidad> RATTATA_AZUL_POKE_HABILIDAD = Arrays.asList(new PokemonHabilidad(RATTATA_AZUL, HABILIDAD_RATTATA_AZUL));
	
	public static final List<PokemonTipo> RATTATA_AZUL_POKE_TIPO = Arrays.asList(new PokemonTipo(RATTATA_AZUL, TIPO_RATTATA_AZUL));

	public static final Optional<Usuario> USUARIO = Optional.of(new Usuario(1,"Cristian"));
	
	public static final List<PokemonUsuario> POKEMONES_USUARIO = Arrays.asList(new PokemonUsuario(1, CHARIZAR, USUARIO.get()));
	
	public static final List<Evolucion> EVOLUCIONES_ALL = Arrays.asList(EVOLUCION_RATTATA.get(), new Evolucion("Charizar evolucionado", "Por la noche", 64, CHARIZAR));
}
