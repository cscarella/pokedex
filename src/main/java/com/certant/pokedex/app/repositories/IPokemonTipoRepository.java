package com.certant.pokedex.app.repositories;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.certant.pokedex.app.entities.PokemonTipo;
import com.certant.pokedex.app.entities.Tipo;

@Repository("pokemonTipoRepository")
public interface IPokemonTipoRepository extends JpaRepository<PokemonTipo, Serializable>{

	@Query("Select pt from PokemonTipo pt JOIN pt.pokemon p where p.id=(:pokemonId)")
	public abstract List<PokemonTipo> obtenerPokemonesPorTipo(int pokemonId);
	
	@Query("Select t from PokemonTipo pt JOIN pt.pokemon p JOIN pt.tipo t where p.id=(:pokemonId)")
	public abstract List<Tipo> obtenerTiposPorPokemon(int pokemonId);
	
	@Query("Select pt from PokemonTipo pt JOIN pt.pokemon p where p.nombre=(:pokemon)")
	public abstract List<PokemonTipo> findByNombre(String pokemon);
}
