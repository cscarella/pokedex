package com.certant.pokedex.app.models;

import java.util.List;

import com.certant.pokedex.app.entities.Evolucion;
import com.certant.pokedex.app.entities.Habilidad;
import com.certant.pokedex.app.entities.Tipo;

public class PokemonModel {

	private int id;

	private String nombre;

	private int nivel;

	private List<Tipo> tipos;

	private List<Habilidad> habilidades;

	private List<Evolucion> evoluciones;

	public PokemonModel() {
	}
	
	public PokemonModel(String nombre, int nivel, List<Tipo> tipos, List<Habilidad> habilidades,
			List<Evolucion> evoluciones) {
		super();
		this.nombre = nombre;
		this.nivel = nivel;
		this.tipos = tipos;
		this.habilidades = habilidades;
		this.evoluciones = evoluciones;
	}
	
	public PokemonModel(int id, String nombre, int nivel, List<Tipo> tipos, List<Habilidad> habilidades,
			List<Evolucion> evoluciones) {
		this(nombre,nivel,tipos,habilidades,evoluciones);
		this.id = id;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<Tipo> getTipos() {
		return tipos;
	}

	public void setTipos(List<Tipo> tipos) {
		this.tipos = tipos;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<Habilidad> getHabilidades() {
		return habilidades;
	}

	public void setHabilidades(List<Habilidad> habilidades) {
		this.habilidades = habilidades;
	}

	public List<Evolucion> getEvoluciones() {
		return evoluciones;
	}

	public void setEvoluciones(List<Evolucion> evoluciones) {
		this.evoluciones = evoluciones;
	}

	public int getNivel() {
		return nivel;
	}

	public void setNivel(int nivel) {
		this.nivel = nivel;
	}



}
