package com.certant.pokedex.app.models.editors;

import java.beans.PropertyEditorSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.certant.pokedex.app.services.ITipoService;

@Component
public class PokemonEditors extends PropertyEditorSupport{

	@Autowired
	private ITipoService tipoService;
	
	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		try {
			int id = Integer.parseInt(text);
			setValue(tipoService.findById(id));
		}catch(NumberFormatException e) {
			setValue(null);
		}
		
	}

	
}
