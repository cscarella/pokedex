package com.certant.pokedex.app.models.editors;

import java.beans.PropertyEditorSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.certant.pokedex.app.services.IHabilidadService;

@Component
public class HabilidadEditors extends PropertyEditorSupport{

	@Autowired
	private IHabilidadService habilidadService;
	
	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		try {
			int id = Integer.parseInt(text);
			setValue(habilidadService.findById(id));
		}catch(NumberFormatException e) {
			setValue(null);
		}
	}

	
}
