package com.certant.pokedex.app;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.util.Arrays;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.certant.pokedex.app.entities.Tipo;
import com.certant.pokedex.app.models.PokemonModel;
import com.certant.pokedex.app.repositories.Datos;
import com.certant.pokedex.app.services.IEvolucionService;
import com.certant.pokedex.app.services.IPokemonService;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
class PokemonRestControllerTest {

	@Autowired
	private MockMvc mvc;
	
	@MockBean
	private IPokemonService service;
	
	@MockBean
	private IEvolucionService evolucionService;
	
	
	ObjectMapper objectMapper;
	
	@BeforeEach
	void setUp() {
		objectMapper = new ObjectMapper();
	}
	
	@Test
	void findByIdTest() throws Exception {
		//given
		when(service.findById(1)).thenReturn(Datos.RATTATA.orElseThrow());
		//when

			mvc.perform(get("/api/1").contentType(MediaType.APPLICATION_JSON))
			//then
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON))
			.andExpect(jsonPath("$.nombre").value("Rattata"))
			.andExpect(jsonPath("$.nivel").value("5"));
		
		verify(service).findById(1);
	}
	
	@Test
	void insertTest() throws Exception {
		PokemonModel model = new PokemonModel();
		model.setNivel(11);
		model.setNombre("Pikachu");
		model.setTipos(Arrays.asList(new Tipo(1, "Tierra")));
		
		//when
		mvc.perform(post("/api/agregar").contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(model)))
		//then
		.andExpect(status().isOk())
		.andExpect(content().contentType(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$.date").value(LocalDate.now().toString()))
		.andExpect(jsonPath("$.insert.nombre").value("Pikachu"))
		.andExpect(jsonPath("$.insert.nivel").value("11"))
		.andExpect(jsonPath("$.mensaje").value("Se insertó correctamente"));
		
	}
	
	@Test
	void listarTest() throws Exception {
		when(service.getAllPokemon()).thenReturn(Datos.LISTA_POKEMONES_BDD);
		
		mvc.perform(get("/api/listar").contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk())
		.andExpect(content().contentType(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$[0].nombre").value("Rattata"))
		.andExpect(jsonPath("$[1].nombre").value("Bulbasaur"))
		.andExpect(jsonPath("$[0].nivel").value("5"))
		.andExpect(jsonPath("$", hasSize(10)));
	}
	
	@Test
	void findEvolucionTest() throws Exception {
		when(evolucionService.findById(1)).thenReturn(Datos.EVOLUCION_RATTATA);
		
		mvc.perform(get("/api/evolucion/1").contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk())
		.andExpect(content().contentType(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$.nombre").value("Raticate"))
		.andExpect(jsonPath("$.tipo").value("Por la noche"))
		.andExpect(jsonPath("$.nivelEvolucion").value("20"))
		.andExpect(jsonPath("$.pokemon.nombre").value("Rattata"));
		
	}
	
	@Test
	void listarEvolucionesTest() throws Exception {
		when(evolucionService.findAll()).thenReturn(Datos.EVOLUCIONES_ALL);
		mvc.perform(get("/api/listarEvoluciones").contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk())
		.andExpect(content().contentType(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$[0].nombre").value("Raticate"))
		.andExpect(jsonPath("$[0].tipo").value("Por la noche"))
		.andExpect(jsonPath("$[1].nombre").value("Charizar evolucionado"))
		.andExpect(jsonPath("$[1].tipo").value("Por la noche"))
		.andExpect(jsonPath("$", hasSize(2)));
		
	}

}
