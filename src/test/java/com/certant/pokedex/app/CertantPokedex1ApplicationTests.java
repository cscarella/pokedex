package com.certant.pokedex.app;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class CertantPokedex1ApplicationTests {
	
	@Test
	void test() {
		assertTrue(true);
	}
//----- tests antiguos -----//
//	@Autowired
//	IPokemonService pokemonService;
//	
//	@Autowired
//	IPokemonTipoRepository pokemonTipoRepository;
//	
//	@Autowired
//	IPokemonHabilidadRepository pokemonHabilidadRepository;
//	
//	@Autowired
//	IEvolucionRepository evolucionRepository;
//	
//	@Autowired
//	ITipoService tipoService;
//	
//	@Autowired
//	IHabilidadService habilidadService;
//	
//
//	@Test
//	@Order(1)
//	void testFindAll() { //test-1
//		List<Pokemon> pokemones = pokemonService.getAllPokemon();
//		assertThat(pokemones).size().isGreaterThan(0);
//		for(Pokemon pokemon: pokemones) {
//			System.out.println(pokemon.toString());
//		}
//	}
//	
//	@Test
//	@Order(2)
//	void testFindByNombreTest2() { //test-2
//		String nombre="Bulbasaur";
//		PokemonModel pokemonModel = new PokemonModel();
//		List<Tipo> tipos = new ArrayList<Tipo>();
//		
//		List<PokemonTipo> pokemones = pokemonTipoRepository.findByNombre(nombre);
//		Pokemon pokemon2=pokemonService.findByNombre(nombre);
//		
//		for(PokemonTipo pokemonTipo : pokemones) {
//			tipos.add(pokemonTipo.getTipo());
//		}
//		pokemonModel.setTipos(tipos);
//		pokemonModel.setNivel(pokemon2.getNivel());
//		pokemonModel.setNombre(pokemon2.getNombre());
//		
//		assertThat(pokemonModel.getNombre()).isEqualTo(nombre);
//		System.out.println(pokemonModel.toString());
//		
//	}
//	
//	@Test
//	@Order(3)
//	void testFindByNombreTest3() { //test-3
//		String nombre="Bulbasaur";
//		PokemonModel pokemonModel = new PokemonModel();
//		List<Habilidad> habilidad = new ArrayList<Habilidad>();
//		
//		List<PokemonHabilidad> pokemones = pokemonHabilidadRepository.findPokemonesHabilidadByNombre(nombre);
//		for(PokemonHabilidad pokemonHabilidad : pokemones) {
//			habilidad.add(pokemonHabilidad.getHabilidad());
//		}
//		pokemonModel.setEvoluciones(evolucionRepository.obtenerEvolucionesPokemon(nombre));
//		pokemonModel.setHabilidades(habilidad);
//		pokemonModel.setNombre(nombre);
//		assertThat(pokemonModel.getNombre()).isEqualTo(nombre);
//		System.out.println(pokemonModel.toString());
//	}
//	
//	@Test
//	@Order(4)
//	void testFindByNombreTest4() { //test-4
//		String nombre="Bulbasaur";
//		List<Tipo> tipos = new ArrayList<Tipo>();
//		PokemonModel pokemonModel = new PokemonModel();
//		
//		List<PokemonTipo> pokemones = pokemonTipoRepository.findByNombre(nombre);
//		Pokemon pokemon2=pokemonService.findByNombre(nombre);
//		
//		for(PokemonTipo pokemonTipo : pokemones) {
//			tipos.add(pokemonTipo.getTipo());
//		}
//		pokemonModel.setNombre(pokemon2.getNombre());
//		pokemonModel.setNivel(pokemon2.getNivel());
//		pokemonModel.setTipos(tipos);
//		pokemonModel.setEvoluciones(evolucionRepository.obtenerEvolucionesPokemon(nombre));
//		System.out.println(pokemonModel.toString());
//	}
//	
//	@Test
//	void testAddPokemon() {
//		PokemonModel pokemon = new PokemonModel();
//		List<Tipo> tipos= new ArrayList<Tipo>();
//		List<Habilidad> habilidades = new ArrayList<Habilidad>();
//		
//		tipos.add(tipoService.findById(1));
//		habilidades.add(habilidadService.findById(1));
//		pokemon.setNombre("Chesnaught");
//		pokemon.setNivel(1);
//		pokemon.setTipos(tipos);
//		pokemon.setHabilidades(habilidades);
//		pokemonService.insertOrUpdate(pokemon);
//		System.out.println(pokemonService.findById(6).getId());
//		assertNotNull(pokemonService.findById(6));
//	}
//	
//	@Test
//	void testEditPokemon() {
//		PokemonModel pokemon = new PokemonModel();
//		Pokemon pokemon2=pokemonService.findByNombre("Chesnaught");
//		List<Tipo> tipos= new ArrayList<Tipo>();
//		
//		tipos.add(tipoService.findById(2));
//		pokemon.setId(pokemon2.getId());
//		pokemon.setNombre("Chespin");
//		pokemon.setNivel(13);
//		pokemon.setTipos(tipos);
//		pokemonService.insertOrUpdate(pokemon);
//	}
}
