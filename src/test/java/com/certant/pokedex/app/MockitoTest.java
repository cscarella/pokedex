package com.certant.pokedex.app;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;
import org.mockito.InOrder;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.certant.pokedex.app.entities.Evolucion;
import com.certant.pokedex.app.entities.Pokemon;
import com.certant.pokedex.app.entities.PokemonHabilidad;
import com.certant.pokedex.app.entities.PokemonTipo;
import com.certant.pokedex.app.entities.PokemonUsuario;
import com.certant.pokedex.app.entities.Tipo;
import com.certant.pokedex.app.entities.Usuario;
import com.certant.pokedex.app.models.PokemonModel;
import com.certant.pokedex.app.repositories.Datos;
import com.certant.pokedex.app.repositories.IEvolucionRepository;
import com.certant.pokedex.app.repositories.IHabilidadRepository;
import com.certant.pokedex.app.repositories.IPokemonHabilidadRepository;
import com.certant.pokedex.app.repositories.IPokemonRepository;
import com.certant.pokedex.app.repositories.IPokemonTipoRepository;
import com.certant.pokedex.app.repositories.IPokemonUsuarioRepository;
import com.certant.pokedex.app.repositories.ITipoRepository;
import com.certant.pokedex.app.repositories.IUsuarioRepository;
import com.certant.pokedex.app.services.IEvolucionService;
import com.certant.pokedex.app.services.IPokemonService;
import com.certant.pokedex.app.services.IPokemonTipoService;
import com.certant.pokedex.app.services.IPokemonUsuarioService;
import com.certant.pokedex.app.services.IUsuarioService;

//@RunWith(SpringRunner.class)
@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class MockitoTest {

	@MockBean
	IPokemonRepository pokemonRepository;

	@MockBean
	IEvolucionRepository evolucionRepository;

	@MockBean
	ITipoRepository tipoRepository;
	
	@MockBean
	IHabilidadRepository habilidadRepository;

	@MockBean
	IPokemonTipoRepository pokemonTipoRepository;
	
	@MockBean
	IPokemonHabilidadRepository pokemonHabilidadRepository;
	
	@MockBean
	IUsuarioRepository usuarioRepository;
	
	@MockBean
	IPokemonUsuarioRepository pokemonUsuarioRepository;

	@Autowired
	IEvolucionService evolucionService;

	@Autowired
	IPokemonService pokemonService;

	@Autowired
	IPokemonTipoService pokemonTipoService;
	
	@Autowired
	IUsuarioService usuarioService;

	@Autowired
	IPokemonUsuarioService pokemonUsuarioService;

	@Test
	@Order(1)
	void findByNameTest() {
		// dado que
		when(pokemonRepository.findByNombre(anyString())).thenReturn(Datos.RATTATA);

		// cuando
		Optional<Pokemon> buscar = pokemonService.findByNombre("Rattata");

		// verificamos
		assertAll(() -> assertTrue(buscar.isPresent()), 
				  () -> assertEquals("Rattata", buscar.get().getNombre()));
		verify(pokemonRepository).findByNombre(ArgumentMatchers.argThat(arg -> arg.equals("Rattata")));
	}

	@Test
	@Order(2)
	void argumentCaptorTest() {
		when(pokemonRepository.findByNombre(anyString())).thenReturn(Datos.RATTATA);
		pokemonService.findByNombre("Rattata");

		ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);

		verify(pokemonRepository).findByNombre(captor.capture());
		assertEquals("Rattata", captor.getValue());
	}

	@Test
	@Order(3)
	void getAllTest() {

		when(pokemonRepository.getAllPokemon()).thenReturn(Datos.LISTA_POKEMONES_BDD);

		assertFalse(pokemonService.getAllPokemon().isEmpty());
		assertEquals(10, pokemonService.getAllPokemon().size());
		verify(pokemonRepository, times(2)).getAllPokemon();
	}

	@Test
	@Order(4)
	void addEvolucionPokemon() {
		// GIVEN PRECONDICIONES
		when(evolucionRepository.save(any(Evolucion.class))).then(new Answer<Evolucion>() {
			int secuencia = 15;

			@Override
			public Evolucion answer(InvocationOnMock invocation) throws Throwable {
				Evolucion evolucion = invocation.getArgument(0);
				evolucion.setId(secuencia++);
				return evolucion;
			}
		});

		// WHEN CUANDO
		Evolucion evoluciona = evolucionService.insertEvolucion(Datos.EVOLUCION);

		// THEN ENTONCES VALIDAMOS CON ASSERTIONS Y VERIFY
		assertNotNull(evoluciona);
		assertEquals(evoluciona.getNombre(), Datos.EVOLUCION.getNombre());
		assertEquals(evoluciona.getNivelEvolucion(), Datos.EVOLUCION.getNivelEvolucion());
		assertEquals(evoluciona.getPokemon(), Datos.EVOLUCION.getPokemon());

	}

	@Test
	@Order(5)
	void obtenerTiposPokemonTest() {
		when(pokemonTipoRepository.obtenerTiposPorPokemon(1)).thenReturn(Datos.TIPOS_RATTATA);
		List<Tipo> tipos = pokemonTipoService.obtenerTiposPorPokemon(1);
		// assertFalse(tipos.isEmpty());
		assertEquals(1, tipos.size());
		verify(pokemonTipoRepository, times(1)).obtenerTiposPorPokemon(1);
	}

	@Test
	@Order(6)
	void deletePokemonTest() {
		when(pokemonRepository.findById(anyInt())).thenReturn(Datos.RATTATA);
		pokemonService.delete(1);
		verify(pokemonRepository).deleteById(1);
	}

	@Test
	@Order(7)
	void deletePokemonFalloTest() {
		when(pokemonRepository.findById(24)).thenReturn(Optional.empty());

		assertThrows(NoSuchElementException.class, () -> {
			pokemonService.delete(24);
		});
		verify(pokemonRepository, never()).deleteById(24);
	}

	@Test
	@Order(8)
	void deleteEvolucionTest() {
		when(evolucionRepository.findById(anyInt())).thenReturn(Datos.EVOLUCION_RATTATA);
		evolucionService.delete(1);
		verify(evolucionRepository).deleteById(1);
	}
	

	@Nested
	class testAgregar {
		@Test
		void addPokemonFalloTest() { // sin tipos
			when(pokemonRepository.save(any(Pokemon.class))).thenReturn(Datos.RATTATA_ROJO_FALLO);

			assertThrows(NoSuchElementException.class, () -> {
				pokemonService.insertOrUpdate(new PokemonModel(Datos.RATTATA_ROJO_FALLO.getNombre(),
						Datos.RATTATA_ROJO_FALLO.getNivel(), null, null, null));
			});
			verify(pokemonRepository, never()).save(any(Pokemon.class)); // verificamos que nunca se llama al repository
																			//si no tiene la lista de tipos
		}

		@Test
		void addPokemonNivelNegativoTest() { // nivel negativo
			Datos.RATTATA_ROJO_FALLO.setNivel(-13);
			when(pokemonRepository.save(any(Pokemon.class))).thenReturn(Datos.RATTATA_ROJO_FALLO);

			assertThrows(NoSuchElementException.class, () -> {
				pokemonService.insertOrUpdate(
						new PokemonModel(Datos.RATTATA_ROJO_FALLO.getNombre(), Datos.RATTATA_ROJO_FALLO.getNivel(),
								Arrays.asList(new Tipo(1, "Normal"), new Tipo(2, "Planta")), null, null));
			});
		}

		@Test
		void addPokemonNoHabilidadesTest() { // insert exitoso de un pokemon sin habilidades, ni evoluciones
			when(pokemonRepository.save(any(Pokemon.class))).thenReturn(Datos.RATTATA_AZUL);
			when(tipoRepository.findById(anyInt())).thenReturn(Datos.TIPO_RATTATA_AZUL);
			
			pokemonService.insertOrUpdate(new PokemonModel(Datos.RATTATA_AZUL.getNombre(),
					Datos.RATTATA_AZUL.getNivel(), Datos.RATTATA_AZUL_TIPOS, null, null));

			InOrder inOrder = inOrder(pokemonRepository, pokemonTipoRepository);
			inOrder.verify(pokemonRepository).save(any(Pokemon.class));
			inOrder.verify(pokemonTipoRepository).save(any(PokemonTipo.class));

			verify(pokemonTipoRepository).save(any(PokemonTipo.class));
			verify(pokemonRepository).save(any(Pokemon.class));

		}
		
		@Test
		void addPokemonCompleto() { // insert exitoso de un pokemon con habilidades, pero sin evoluciones
			when(pokemonRepository.save(any(Pokemon.class))).thenReturn(Datos.RATTATA_AZUL);
			when(tipoRepository.findById(anyInt())).thenReturn(Datos.TIPO_RATTATA_AZUL);
			when(habilidadRepository.findById(anyInt())).thenReturn(Datos.HABILIDAD_RATTATA_AZUL);
			
			pokemonService.insertOrUpdate(new PokemonModel(Datos.RATTATA_AZUL.getNombre(),
					Datos.RATTATA_AZUL.getNivel(), Datos.RATTATA_AZUL_TIPOS, Datos.RATTATA_AZUL_HABILIDADES, null));

			InOrder inOrder = inOrder(pokemonRepository, pokemonTipoRepository, pokemonHabilidadRepository);
			inOrder.verify(pokemonRepository).save(any(Pokemon.class));
			inOrder.verify(pokemonTipoRepository).save(any(PokemonTipo.class));
			inOrder.verify(pokemonHabilidadRepository).save(any(PokemonHabilidad.class));

			verify(pokemonTipoRepository).save(any(PokemonTipo.class));
			verify(pokemonRepository).save(any(Pokemon.class));
			verify(pokemonHabilidadRepository).save(any(PokemonHabilidad.class));
		}
		
		@Test
		void obtenerUsuario() {
			when(usuarioRepository.findById(anyInt())).thenReturn(Datos.USUARIO);

			Usuario user= usuarioService.findById(1);
			
			assertNotNull(user);
			verify(usuarioRepository).findById(anyInt());
		}
		
		@Test
		void obtenerPokemonesPorUsuario() {
			//when(usuarioRepository.findById(anyInt())).thenReturn(Datos.USUARIO);
			when(pokemonUsuarioRepository.obtenerPokemonesPorUsuario(anyInt())).thenReturn(Datos.POKEMONES_USUARIO);
			
			List<PokemonUsuario> pokemonesByUsuario= pokemonUsuarioService.getAllPokemonesByUsuario(1);
			assertFalse(pokemonesByUsuario.isEmpty());
			assertEquals(1,pokemonesByUsuario.size());
			verify(pokemonUsuarioRepository).obtenerPokemonesPorUsuario(anyInt());
		}
		
		
	}

}
