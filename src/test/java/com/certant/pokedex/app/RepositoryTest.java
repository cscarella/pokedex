package com.certant.pokedex.app;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.certant.pokedex.app.entities.Evolucion;
import com.certant.pokedex.app.entities.Pokemon;
import com.certant.pokedex.app.repositories.IEvolucionRepository;
import com.certant.pokedex.app.repositories.IPokemonHabilidadRepository;
import com.certant.pokedex.app.repositories.IPokemonRepository;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@SpringBootTest
class RepositoryTest {

	@Autowired
	IPokemonRepository pokemonRepository;
	
	@Autowired
	IPokemonHabilidadRepository pokemonHabilidadRepository;

	@Autowired
	IEvolucionRepository evolucionRepository;

	Pokemon pokemon;
	
	Optional<Pokemon> pokemonOptional;
	
	@BeforeEach
	void init() {
		pokemonOptional=Optional.empty();
		pokemon = new Pokemon();
	}

	@Test
	@Order(1)
	void findByNameThrowExceptionTest() {
		pokemonOptional = pokemonRepository.findByNombre("Rattatataa");
		assertThrows(NoSuchElementException.class, ()->{
			pokemonOptional.orElseThrow(() -> new NoSuchElementException());
		});
	}
	
	@Test
	@Order(2)
	void getAllTest() {
		List<Pokemon> lista= pokemonRepository.getAllPokemon();
		assertFalse(lista.isEmpty());
		assertEquals(10, lista.size());
	}
	
	@Test
	@Order(3)
	void addPokemonTest() {

		pokemon= new Pokemon("Pikachu enemigo", 34);
		pokemonRepository.save(pokemon);
		
		//cuando
		pokemon= pokemonRepository.findById(11).orElseThrow(() -> new NoSuchElementException());
		
		//verificamos
		assertEquals("Pikachu enemigo", pokemon.getNombre());
		assertEquals(34,pokemon.getNivel());
	}
	
	@Test
	@Order(4)
	void updatePokemonTest() {
		//Insertamos un nuevo pokemon en la BDD
		pokemon= new Pokemon("Pikachu Rojo", 47);
		pokemonRepository.save(pokemon); 

		//editamos el pokemon y actualizamos el registro
		pokemon.setNivel(60);
		pokemon.setNombre("Pikachu colorado");
		pokemonRepository.save(pokemon);
		
		//buscamos el pokemon que actualizamos
		pokemon= pokemonRepository.findByNombre("Pikachu colorado").orElseThrow(() -> new NoSuchElementException());

		//verificamos
		assertAll(() -> assertEquals("Pikachu colorado", pokemon.getNombre()),
				  () -> assertEquals(60,pokemon.getNivel()));
	}
	
	@Test
	@Order(5)
	void agregarEvolucion() {
		//cuando
		pokemon= pokemonRepository.findById(8).orElseThrow(() -> new NoSuchElementException());
		Evolucion evolucion= new Evolucion("Caterpie Evolucionado", "Tierra", 70, pokemon);
		
		//verificamos cuantos evoluciones tiene el pokemon
		assertEquals(2,evolucionRepository.obtenerEvolucionesPokemon(8).size());
		
		//cuando
		evolucionRepository.save(evolucion);
		
		//verificamos que el Caterpie tiene 3 evoluciones cargadas
		assertEquals(3,evolucionRepository.obtenerEvolucionesPokemon(8).size());
				
	}
	
	@Test
	@Order(6)
	void testFindById() {
		pokemonOptional= pokemonRepository.findById(1);
		assertTrue(pokemonOptional.isPresent());
		assertEquals("Rattata", pokemonOptional.get().getNombre());
	}
	
	@Test
	@Order(7)
	void testDeletePokemon() {
		//buscamos el pokemon y su evolucion por id
		pokemon= pokemonRepository.findById(5).orElseThrow(() -> new NoSuchElementException());
		Optional<Evolucion> evolucion = evolucionRepository.findById(5);
		
		//verificamos si son iguales los datos
		assertEquals("Primeape", evolucion.get().getNombre());
		assertEquals("Mankey", pokemon.getNombre());
		
		//eliminamos y verificamos si fueron eliminados correctamente
		evolucionRepository.delete(evolucion.get());
		pokemonRepository.delete(pokemon);
		assertThrows(NoSuchElementException.class, ()->{
			pokemonRepository.findByNombre("Mankey").orElseThrow(() -> new NoSuchElementException());
		});
		assertEquals(11, pokemonRepository.getAllPokemon().size());
	}
	
	//en el delete tira error porque se ejecuta primero este metodo antes que el addPokemon
	// por lo que da que la cantidad de pokemones es incorrecta.
	//lo mismo pasa en el add porque se ejecuta primero el update en vez de el add
	//secuencia:
	//			-findById
	//			-getAllTest
	//			-updatePokemonTest
	//			-agregarEvolucion
	//			-TestDeletePokemon
	//			-findByNameThrowExceptionTest
	//			-addPokemonTest
}
